# Advent 2021

See <https://adventofcode.com/>

## Installation

```sh
asdf install
mix local.rebar
mix local.hex
mix deps.get
```

## Usage

- Tests: `mix check`
- Day 01: `mix sonar_sweep`
