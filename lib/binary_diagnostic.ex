defmodule BinaryDiagnostics do
  @moduledoc """
  Find the problem
  """

  defmodule PartOne do
    @moduledoc """
    First puzzle
    """

    @spec run([String.t()]) :: integer()
    def run(input) do
      half_length = length(input) / 2
      sum = sum_per_column(input)

      gamma =
        sum
        |> Enum.map(fn sum ->
          if sum < half_length do
            0
          else
            1
          end
        end)
        |> Enum.join()
        |> Integer.parse(2)
        |> elem(0)

      epsilon =
        sum
        |> Enum.map(fn sum ->
          if sum < half_length do
            1
          else
            0
          end
        end)
        |> Enum.join()
        |> Integer.parse(2)
        |> elem(0)

      gamma * epsilon
    end

    defp sum_per_column(input) do
      Enum.reduce(input, nil, &process_row/2)
    end

    defp process_row(row, nil), do: as_bits(row)

    defp process_row(row, sums) do
      bits = as_bits(row)
      Enum.zip_with([bits, sums], fn [b, s] -> b + s end)
    end

    defp as_bits(row) do
      row
      |> String.graphemes()
      |> Enum.map(&String.to_integer/1)
    end
  end

  defmodule PartTwo do
    @moduledoc """
    Second puzzle
    """

    @spec run([String.t()]) :: integer()
    def run(input) do
      oxygen =
        %{candidates: input, index: 0, most_common?: true}
        |> find_bit()
        |> Integer.parse(2)
        |> elem(0)

      co2 =
        %{candidates: input, index: 0, most_common?: false}
        |> find_bit()
        |> Integer.parse(2)
        |> elem(0)

      oxygen * co2
    end

    defp find_bit(%{candidates: [solution | []]}), do: solution

    defp find_bit(%{candidates: candidates, index: index, most_common?: most_common?}) do
      bit_value =
        candidates
        |> Enum.map(&String.slice(&1, index, 1))
        |> Enum.frequencies()
        |> get_most_or_least_common(most_common?)
        |> elem(0)

      candidates =
        Enum.filter(candidates, fn candidate -> String.slice(candidate, index, 1) == bit_value end)

      find_bit(%{candidates: candidates, index: index + 1, most_common?: most_common?})
    end

    defp get_most_or_least_common(%{"0" => c, "1" => c}, true), do: {"1", c}
    defp get_most_or_least_common(frequencies, true), do: Enum.max_by(frequencies, &elem(&1, 1))
    defp get_most_or_least_common(%{"0" => c, "1" => c}, false), do: {"0", c}
    defp get_most_or_least_common(frequencies, false), do: Enum.min_by(frequencies, &elem(&1, 1))
  end
end
