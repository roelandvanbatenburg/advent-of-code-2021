defmodule Chiton do
  @moduledoc """
  Decode the display
  """

  defmodule PartOne do
    @moduledoc """
    First puzzle
    """

    @spec run([String.t()]) :: integer()
    def run(input) do
      dimension = length(input) - 1
      costs = Chiton.parse(input, dimension)

      costs
      |> Chiton.find_safest_path(dimension)
      |> Chiton.score(costs)
    end
  end

  defmodule PartTwo do
    @moduledoc """
    Second puzzle
    """

    @spec run([String.t()]) :: integer()
    def run(input) do
      dimension = length(input) - 1

      costs =
        input
        |> Chiton.parse(dimension)
        |> copy(dimension, 0, 1)

      costs
      |> Chiton.find_safest_path((dimension + 1) * 5 - 1)
      |> Chiton.score(costs)
    end

    defp copy(costs, _dimension, 4, 5), do: costs
    defp copy(costs, dimension, y, 5), do: copy(costs, dimension, y + 1, 0)

    defp copy(costs, dimension, y, x) do
      diff = y + x

      0..dimension
      |> Enum.reduce(costs, fn ys, acc ->
        0..dimension
        |> Enum.reduce(acc, fn xs, acc ->
          Map.put(
            acc,
            {ys + y * (dimension + 1), xs + x * (dimension + 1)},
            value(Map.fetch!(costs, {ys, xs}), diff)
          )
        end)
      end)
      |> copy(dimension, y, x + 1)
    end

    defp value(original, diff) do
      sum = original + diff

      if sum < 10 do
        sum
      else
        value(original - 9, diff)
      end
    end
  end

  @type location :: {integer(), integer()}
  @type costs :: %{location() => integer()}

  @spec parse([String.t()], integer()) :: costs()
  def parse(input, dimension) do
    0..dimension
    |> Enum.reduce(%{}, fn y, acc ->
      line =
        input
        |> Enum.at(y)
        |> String.graphemes()

      0..dimension
      |> Enum.reduce(acc, &Map.put(&2, {y, &1}, String.to_integer(Enum.at(line, &1))))
    end)
  end

  @spec find_safest_path(costs(), integer()) :: [location()]
  def(find_safest_path(costs, dimension)) do
    costs
    |> Enum.reduce(Graph.new(), fn {{y, x}, cost}, graph ->
      graph
      |> add_edge(:top, y, x, dimension, cost)
      |> add_edge(:right, y, x, dimension, cost)
      |> add_edge(:bottom, y, x, dimension, cost)
      |> add_edge(:left, y, x, dimension, cost)
    end)
    |> Graph.dijkstra({0, 0}, {dimension, dimension})
  end

  defp add_edge(graph, :top, 0, _x, _m, _cost), do: graph
  defp add_edge(graph, :bottom, y, _x, y, _cost), do: graph
  defp add_edge(graph, :right, _y, x, x, _cost), do: graph
  defp add_edge(graph, :left, _y, 0, _m, _cost), do: graph

  defp add_edge(graph, :top, y, x, _m, cost),
    do: Graph.add_edge(graph, {y, x}, {y - 1, x}, weight: cost)

  defp add_edge(graph, :bottom, y, x, _m, cost),
    do: Graph.add_edge(graph, {y, x}, {y + 1, x}, weight: cost)

  defp add_edge(graph, :right, y, x, _m, cost),
    do: Graph.add_edge(graph, {y, x}, {y, x + 1}, weight: cost)

  defp add_edge(graph, :left, y, x, _m, cost),
    do: Graph.add_edge(graph, {y, x}, {y, x - 1}, weight: cost)

  @spec score([location()], costs) :: integer()
  def score(path, costs) do
    path
    |> Enum.drop(1)
    |> Enum.reduce(0, fn point, risk ->
      risk + Map.fetch!(costs, point)
    end)
  end
end
