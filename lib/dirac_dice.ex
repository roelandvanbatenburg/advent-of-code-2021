defmodule DiracDice do
  @moduledoc """
  Find the keys
  """

  defmodule PartOne do
    @moduledoc """
    First puzzle
    """

    @spec run({integer(), integer()}) :: integer()
    def run({p1, p2}) do
      play(p1, p2)
    end

    defp play(
           position_current,
           position_other,
           score_current \\ 0,
           score_other \\ 0,
           die \\ 1,
           throws \\ 0
         )

    defp play(position_current, position_other, score_current, score_other, die, throws) do
      {steps, die} = roll(die)
      throws = throws + 3
      position_current = DiracDice.move(position_current, steps)
      score_current = score_current + position_current

      if score_current >= 1000 do
        throws * score_other
      else
        play(position_other, position_current, score_other, score_current, die, throws)
      end
    end

    defp roll(98), do: {98 + 99 + 100, 1}
    defp roll(99), do: {99 + 100 + 1, 2}
    defp roll(100), do: {100 + 1 + 2, 3}
    defp roll(d), do: {d + d + 1 + d + 2, d + 3}
  end

  defmodule PartTwo do
    @moduledoc """
    Second puzzle
    """

    @freqs %{3 => 1, 4 => 3, 5 => 6, 6 => 7, 7 => 6, 8 => 3, 9 => 1}

    @spec run({integer(), integer()}) :: integer()
    def run({p1, p2}) do
      {w1, w2} = play({p1, 0}, {p2, 0}, 1, {0, 0}, 1)
      max(w1, w2)
    end

    defp play({p1, s1}, other, 1, {w1, w2}, factor) do
      Enum.reduce(@freqs, {w1, w2}, fn {step, freq}, {w1, w2} ->
        p1 = DiracDice.move(p1, step)
        s1 = s1 + p1

        if s1 >= 21 do
          {w1 + factor * freq, w2}
        else
          play({p1, s1}, other, 0, {w1, w2}, factor * freq)
        end
      end)
    end

    defp play(other, {p2, s2}, 0, {w1, w2}, factor) do
      Enum.reduce(@freqs, {w1, w2}, fn {step, freq}, {w1, w2} ->
        p2 = DiracDice.move(p2, step)
        s2 = s2 + p2

        if s2 >= 21 do
          {w1, w2 + factor * freq}
        else
          play(other, {p2, s2}, 1, {w1, w2}, factor * freq)
        end
      end)
    end
  end

  def move(position, steps) do
    case rem(position + steps, 10) do
      0 -> 10
      n -> n
    end
  end
end
