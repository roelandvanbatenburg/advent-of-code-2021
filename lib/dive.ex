defmodule Dive do
  @moduledoc """
  Find the keys
  """

  defmodule PartOne do
    @moduledoc """
    First puzzle
    """

    @spec run([String.t()]) :: integer()
    def run(input) do
      {horizontal, depth} = Enum.reduce(input, {0, 0}, &dive/2)
      horizontal * depth
    end

    defp dive("forward " <> units, {h, d}), do: {h + String.to_integer(units), d}
    defp dive("down " <> units, {h, d}), do: {h, d + String.to_integer(units)}
    defp dive("up " <> units, {h, d}), do: {h, d - String.to_integer(units)}
  end

  defmodule PartTwo do
    @moduledoc """
    Second puzzle
    """

    @spec run([String.t()]) :: integer()
    def run(input) do
      {horizontal, depth, _aim} = Enum.reduce(input, {0, 0, 0}, &dive/2)
      horizontal * depth
    end

    defp dive("forward " <> units, {h, d, a}) do
      units = String.to_integer(units)
      {h + units, d + a * units, a}
    end

    defp dive("down " <> units, {h, d, a}), do: {h, d, a + String.to_integer(units)}
    defp dive("up " <> units, {h, d, a}), do: {h, d, a - String.to_integer(units)}
  end
end
