defmodule DumboOctopus do
  @moduledoc """
  Count the flahes
  """

  defmodule PartOne do
    @moduledoc """
    First puzzle
    """

    @spec run([[integer()]]) :: integer()
    def run(input) do
      octopuses = DumboOctopus.parse(input)
      step({octopuses, 0}, 100)
    end

    defp step({_octopuses, flashes}, 0), do: flashes

    defp step({octopuses, flashes}, steps) do
      octopuses
      |> DumboOctopus.gain_energy()
      |> DumboOctopus.handle_flashes(flashes)
      |> step(steps - 1)
    end
  end

  defmodule PartTwo do
    @moduledoc """
    Second puzzle
    """

    @spec run([integer()]) :: integer()
    def run(input) do
      input
      |> DumboOctopus.parse()
      |> step()
    end

    defp step(octopuses, steps \\ 1) do
      {octopuses, _flashes} =
        octopuses
        |> DumboOctopus.gain_energy()
        |> DumboOctopus.handle_flashes()

      if DumboOctopus.all_flashed?(octopuses) do
        steps
      else
        step(octopuses, steps + 1)
      end
    end
  end

  @spec parse([[integer()]]) :: %{{integer(), integer()} => integer()}
  def parse(input) do
    0..(length(input) - 1)
    |> Enum.reduce(%{}, fn y, acc ->
      line = Enum.at(input, y)

      0..(length(line) - 1)
      |> Enum.reduce(acc, &Map.put(&2, {y, &1}, Enum.at(line, &1)))
    end)
  end

  def gain_energy(octopuses) do
    octopuses
    |> Enum.map(&{elem(&1, 0), elem(&1, 1) + 1})
    |> Map.new()
  end

  def handle_flashes(octopuses, flashes \\ 0) do
    case Enum.find(octopuses, &(elem(&1, 1) > 9)) do
      nil -> {octopuses, flashes}
      {position, _energy} -> flash(octopuses, position, flashes + 1)
    end
  end

  defp flash(octopuses, position, flashes) do
    octopuses
    |> Map.update!(position, fn _ -> 0 end)
    |> update_neighbours(position)
    |> handle_flashes(flashes)
  end

  defp update_neighbours(octopuses, {y, x}) do
    octopuses
    |> update_neighbour(y - 1, x - 1)
    |> update_neighbour(y - 1, x)
    |> update_neighbour(y - 1, x + 1)
    |> update_neighbour(y, x - 1)
    |> update_neighbour(y, x + 1)
    |> update_neighbour(y + 1, x - 1)
    |> update_neighbour(y + 1, x)
    |> update_neighbour(y + 1, x + 1)
  end

  defp update_neighbour(octopuses, y, x) do
    target_energy = Map.get(octopuses, {y, x}, 0)

    if target_energy > 0 do
      Map.put(octopuses, {y, x}, target_energy + 1)
    else
      octopuses
    end
  end

  def all_flashed?(octopuses) do
    Enum.all?(octopuses, fn {_position, value} -> value == 0 end)
  end
end
