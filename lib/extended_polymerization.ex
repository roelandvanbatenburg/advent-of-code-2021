defmodule ExtendedPolymerization do
  @moduledoc """
  Fix the hull
  """

  @type template :: {%{String.t() => integer()}, String.t()}
  @type insertions :: %{String.t() => [String.t()]}
  @type data :: {template(), insertions()}

  defmodule PartOne do
    @moduledoc """
    First puzzle
    """

    @spec run(String.t()) :: integer()
    def run(input) do
      input
      |> ExtendedPolymerization.parse()
      |> ExtendedPolymerization.step(10)
      |> ExtendedPolymerization.score()
    end
  end

  defmodule PartTwo do
    @moduledoc """
    Second puzzle
    """

    @spec run(String.t()) :: integer()
    def run(input) do
      input
      |> ExtendedPolymerization.parse()
      |> ExtendedPolymerization.step(40)
      |> ExtendedPolymerization.score()
    end
  end

  @spec parse(String.t()) :: data()
  def parse(input) do
    [template, insertions] = String.split(input, "\n\n")
    {parse_template(template), parse_insertions(insertions)}
  end

  defp parse_template(input) do
    parts =
      input
      |> String.trim()
      |> String.graphemes()

    {parts
     |> Stream.chunk_every(2, 1, :discard)
     |> Stream.map(&Enum.join/1)
     |> Enum.frequencies(), List.last(parts)}
  end

  defp parse_insertions(input) do
    input
    |> String.trim()
    |> String.split("\n")
    |> Enum.reduce(%{}, fn insertion, acc ->
      [source, results] = parse_insertion(insertion)
      Map.put(acc, source, results)
    end)
  end

  defp parse_insertion(insertion), do: String.split(insertion, " -> ")

  @spec step(data(), integer()) :: template()
  def step({template, _insertions}, 0), do: template

  def step({{frequencies, last}, insertions}, steps) do
    frequencies =
      frequencies
      |> Enum.reduce(%{}, fn {molecule, frequency}, acc ->
        [m1, m2] = String.graphemes(molecule)
        insert = Map.fetch!(insertions, molecule)

        acc
        |> Map.update(m1 <> insert, frequency, &Kernel.+(&1, frequency))
        |> Map.update(insert <> m2, frequency, &Kernel.+(&1, frequency))
      end)

    step({{frequencies, last}, insertions}, steps - 1)
  end

  @spec score(template()) :: integer()
  def score({frequencies, last}) do
    {min, max} =
      frequencies
      |> Enum.reduce(%{last => 1}, fn {molecule, count}, acc ->
        Map.update(acc, String.at(molecule, 0), count, &Kernel.+(&1, count))
      end)
      |> Enum.min_max_by(&elem(&1, 1))

    elem(max, 1) - elem(min, 1)
  end
end
