defmodule GiantSquid do
  @moduledoc """
  Play bingo
  """

  alias GiantSquid.BingoCard

  @spec parse(String.t()) :: {[integer()], [BingoCard.t()]}
  def parse(input) do
    [numbers | cards] = String.split(input, "\n\n")

    numbers =
      numbers
      |> String.split(",")
      |> Enum.map(&String.to_integer/1)

    {numbers, Enum.map(cards, &BingoCard.init/1)}
  end

  defmodule PartOne do
    @moduledoc """
    First puzzle
    """

    @spec run({[integer()], [BingoCard.t()]}) :: integer()
    def run({numbers, cards}) do
      numbers
      |> Enum.reduce_while(cards, fn number, cards ->
        cards = Enum.map(cards, &BingoCard.mark(&1, number))

        case Enum.find(cards, &BingoCard.wins?/1) do
          nil -> {:cont, cards}
          winning_card -> {:halt, BingoCard.score(winning_card, number)}
        end
      end)
    end
  end

  defmodule PartTwo do
    @moduledoc """
    Second puzzle
    """

    @spec run({[integer()], [BingoCard.t()]}) :: integer()
    def run({numbers, cards}) do
      numbers
      |> Enum.reduce_while(cards, fn number, cards ->
        cards = Enum.map(cards, &BingoCard.mark(&1, number))
        remaining_cards = Enum.reject(cards, &BingoCard.wins?/1)

        if Enum.empty?(remaining_cards) do
          {:halt, BingoCard.score(List.first(cards), number)}
        else
          {:cont, remaining_cards}
        end
      end)
    end
  end
end
