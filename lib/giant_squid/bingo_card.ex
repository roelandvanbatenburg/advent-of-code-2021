defmodule GiantSquid.BingoCard do
  @moduledoc """
  Handling the bingo card for the giant squid, because they keep getting stuck on it
  """
  @type t :: %{required(position()) => bingo_number()}
  @type position :: {integer(), integer()}
  @type bingo_number :: %{number: integer(), marked: boolean()}

  @card_size 5
  @card_max_index @card_size - 1

  @spec init(String.t()) :: t()
  def init(input) do
    {_, card} =
      input
      |> String.split("\n")
      |> Enum.reduce({0, %{}}, fn line, {y, acc} ->
        {_, acc} =
          line
          |> String.split()
          |> Enum.reduce({0, acc}, fn number, {x, acc} ->
            {x + 1, Map.put(acc, {y, x}, %{number: String.to_integer(number), marked: false})}
          end)

        {y + 1, acc}
      end)

    card
  end

  @spec mark(t(), integer()) :: t()
  def mark(card, target_number) do
    case Enum.find(card, fn {_position, %{number: number}} -> number == target_number end) do
      nil -> card
      {position, bingo_number} -> Map.put(card, position, %{bingo_number | marked: true})
    end
  end

  @spec wins?(t()) :: boolean()
  def wins?(card) do
    Enum.any?(0..@card_max_index, &row_completed(&1, card)) ||
      Enum.any?(0..@card_max_index, &column_completed(&1, card))
  end

  defp row_completed(y, card) do
    Enum.all?(0..@card_max_index, &marked?(card, {y, &1}))
  end

  defp column_completed(x, card) do
    Enum.all?(0..@card_max_index, &marked?(card, {&1, x}))
  end

  defp marked?(card, position), do: Map.fetch!(card, position).marked

  @spec score(t(), integer()) :: integer()
  def score(card, current_number) do
    card_score =
      card
      |> Stream.reject(fn {_position, %{marked: marked}} -> marked end)
      |> Stream.map(fn {_position, %{number: number}} -> number end)
      |> Enum.sum()

    card_score * current_number
  end
end
