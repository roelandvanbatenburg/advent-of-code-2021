defmodule HydrothermalVenture do
  @moduledoc """
  Find a safe route
  """
  defmodule PartOne do
    @moduledoc """
    First puzzle
    """

    @spec run([String.t()]) :: integer()
    def run(input) do
      input
      |> Enum.map(&HydrothermalVenture.parse_line/1)
      |> Enum.reduce(%{}, &add_vent_to_map/2)
      |> Enum.filter(fn {_location, count} -> count >= 2 end)
      |> length()
    end

    defp add_vent_to_map({{x, y1}, {x, y2}}, acc) do
      y1..y2
      |> Enum.reduce(acc, &Map.update(&2, {x, &1}, 1, fn count -> count + 1 end))
    end

    defp add_vent_to_map({{x1, y}, {x2, y}}, acc) do
      x1..x2
      |> Enum.reduce(acc, &Map.update(&2, {&1, y}, 1, fn count -> count + 1 end))
    end

    defp add_vent_to_map(_, acc), do: acc
  end

  defmodule PartTwo do
    @moduledoc """
    Second puzzle
    """

    @spec run([String.t()]) :: integer()
    def run(input) do
      input
      |> Enum.map(&HydrothermalVenture.parse_line/1)
      |> Enum.reduce(%{}, &add_vent_to_map/2)
      |> Enum.filter(fn {_location, count} -> count >= 2 end)
      |> length()
    end

    defp add_vent_to_map({{x, y1}, {x, y2}}, acc) do
      y1..y2
      |> Enum.reduce(acc, &Map.update(&2, {x, &1}, 1, fn count -> count + 1 end))
    end

    defp add_vent_to_map({{x1, y}, {x2, y}}, acc) do
      x1..x2
      |> Enum.reduce(acc, &Map.update(&2, {&1, y}, 1, fn count -> count + 1 end))
    end

    defp add_vent_to_map({{x1, y1}, {x2, y2}}, acc) do
      shared_direction = (x2 > x1 and y2 > y1) or (x2 < x1 and y2 < y1)

      0..(x2 - x1)
      |> Enum.reduce(
        acc,
        &Map.update(&2, coordinate(x1, y1, &1, shared_direction), 1, fn count -> count + 1 end)
      )
    end

    defp coordinate(x, y, step, true), do: {x + step, y + step}
    defp coordinate(x, y, step, false), do: {x + step, y - step}
  end

  @spec parse_line(String.t()) :: {{integer(), integer()}, {integer(), integer()}}
  def parse_line(line) do
    [start, stop] = String.split(line, " -> ")
    {coordinate(start), coordinate(stop)}
  end

  defp coordinate(point) do
    [x, y] = String.split(point, ",")
    {String.to_integer(x), String.to_integer(y)}
  end
end
