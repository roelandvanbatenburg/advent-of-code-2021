defmodule Lanternfish do
  @moduledoc """
  Count the fish
  """

  defmodule PartOne do
    @moduledoc """
    First puzzle
    """

    @spec run([integer()]) :: integer()
    def run(input) do
      input
      |> Lanternfish.day_to_number_of_fish_map()
      |> Lanternfish.step(80)
      |> Lanternfish.number_of_fish()
    end
  end

  defmodule PartTwo do
    @moduledoc """
    Second puzzle
    """

    @spec run([integer()]) :: integer()
    def run(input) do
      input
      |> Lanternfish.day_to_number_of_fish_map()
      |> Lanternfish.step(256)
      |> Lanternfish.number_of_fish()
    end
  end

  @spec day_to_number_of_fish_map([integer()]) :: %{integer() => integer()}
  def day_to_number_of_fish_map(days) do
    Enum.reduce(days, %{}, &Map.update(&2, &1, 1, fn fish -> fish + 1 end))
  end

  @spec step(%{integer() => integer()}, integer()) :: %{integer() => integer()}
  def step(map, 0), do: map

  def step(map, steps) do
    map
    |> Enum.reduce(%{}, &handle_step/2)
    |> step(steps - 1)
  end

  defp handle_step({0, fish}, acc) do
    acc
    |> Map.put(8, fish)
    |> Map.update(6, fish, &(&1 + fish))
  end

  defp handle_step({day, fish}, acc), do: Map.update(acc, day - 1, fish, &(&1 + fish))

  @spec number_of_fish(%{integer() => integer()}) :: integer()
  def number_of_fish(map), do: Enum.reduce(map, 0, &(&2 + elem(&1, 1)))
end
