defmodule Mix.Tasks.Chiton do
  use Mix.Task

  @shortdoc "Day 15"

  @moduledoc """
  Given the input file priv/input_15.txt find the safest route

  ## Example

  mix chiton (--part2)
  """

  alias Chiton.{PartOne, PartTwo}

  def run(args) do
    "priv/input_15.txt"
    |> File.stream!()
    |> Stream.map(&String.trim_trailing/1)
    |> Enum.to_list()
    |> run(args)
    |> Integer.to_string()
    |> Mix.shell().info()
  end

  defp run(input, []), do: PartOne.run(input)
  defp run(input, _), do: PartTwo.run(input)
end
