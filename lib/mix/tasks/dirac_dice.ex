defmodule Mix.Tasks.DiracDice do
  use Mix.Task

  @shortdoc "Day 21"

  @moduledoc """
  Given the input values play the game

  ## Example

  mix dirac_dice (--part2)
  """

  alias DiracDice.{PartOne, PartTwo}

  def run(args) do
    {6, 9}
    |> run(args)
    |> Integer.to_string()
    |> Mix.shell().info()
  end

  defp run(input, []), do: PartOne.run(input)
  defp run(input, _), do: PartTwo.run(input)
end
