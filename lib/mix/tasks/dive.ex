defmodule Mix.Tasks.Dive do
  use Mix.Task

  @shortdoc "Day 02"

  @moduledoc """
  Given the input file priv/input_02.txt find the keys to the sleigh

  ## Example

  mix dive (--part2)
  """

  alias Dive.{PartOne, PartTwo}

  def run(args) do
    "priv/input_02.txt"
    |> File.stream!()
    |> Stream.map(&String.trim_trailing/1)
    |> Enum.to_list()
    |> run(args)
    |> Integer.to_string()
    |> Mix.shell().info()
  end

  defp run(input, []), do: PartOne.run(input)
  defp run(input, _), do: PartTwo.run(input)
end
