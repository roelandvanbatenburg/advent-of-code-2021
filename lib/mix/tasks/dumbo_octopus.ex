defmodule Mix.Tasks.DumboOctopus do
  use Mix.Task

  @shortdoc "Day 11"

  @moduledoc """
  Given the input file priv/input_11.txt count the flashes

  ## Example

  mix dumbo_octopus (--part2)
  """

  alias DumboOctopus.{PartOne, PartTwo}

  def run(args) do
    "priv/input_11.txt"
    |> File.stream!()
    |> Stream.map(&String.trim_trailing/1)
    |> Stream.map(&String.graphemes/1)
    |> Stream.map(fn line -> Enum.map(line, &String.to_integer/1) end)
    |> Enum.to_list()
    |> run(args)
    |> Integer.to_string()
    |> Mix.shell().info()
  end

  defp run(input, []), do: PartOne.run(input)
  defp run(input, _), do: PartTwo.run(input)
end
