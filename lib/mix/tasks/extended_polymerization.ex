defmodule Mix.Tasks.ExtendedPolymerization do
  use Mix.Task

  @shortdoc "Day 13"

  @moduledoc """
  Given the input file priv/input_14.txt find the solution

  ## Example

  mix extended_polymerization (--part2)
  """

  alias ExtendedPolymerization.{PartOne, PartTwo}

  def run(args) do
    "priv/input_14.txt"
    |> File.stream!()
    |> Enum.join("")
    |> run(args)
    |> Integer.to_string()
    |> Mix.shell().info()
  end

  defp run(input, []), do: PartOne.run(input)
  defp run(input, _), do: PartTwo.run(input)
end
