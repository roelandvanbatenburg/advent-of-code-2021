defmodule Mix.Tasks.GiantSquid do
  use Mix.Task

  @shortdoc "Day 03"

  @moduledoc """
  Given the input file priv/input_04.txt play bingo

  ## Example

  mix giant_squid (--part2)
  """

  alias GiantSquid.{PartOne, PartTwo}

  def run(args) do
    "priv/input_04.txt"
    |> File.read!()
    |> GiantSquid.parse()
    |> run(args)
    |> Integer.to_string()
    |> Mix.shell().info()
  end

  defp run(input, []), do: PartOne.run(input)
  defp run(input, _), do: PartTwo.run(input)
end
