defmodule Mix.Tasks.HydrothermalVenture do
  use Mix.Task

  @shortdoc "Day 05"

  @moduledoc """
  Given the input file priv/input_05.txt find a safe route

  ## Example

  mix hydrothermal_venture (--part2)
  """

  alias HydrothermalVenture.{PartOne, PartTwo}

  def run(args) do
    "priv/input_05.txt"
    |> File.stream!()
    |> Stream.map(&String.trim_trailing/1)
    |> Enum.to_list()
    |> run(args)
    |> Integer.to_string()
    |> Mix.shell().info()
  end

  defp run(input, []), do: PartOne.run(input)
  defp run(input, _), do: PartTwo.run(input)
end
