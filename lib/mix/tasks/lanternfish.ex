defmodule Mix.Tasks.Lanternfish do
  use Mix.Task

  @shortdoc "Day 03"

  @moduledoc """
  Given the input file priv/input_06.txt count the fish

  ## Example

  mix lanternfish (--part2)
  """

  alias Lanternfish.{PartOne, PartTwo}

  def run(args) do
    "priv/input_06.txt"
    |> File.read!()
    |> String.trim()
    |> String.split(",")
    |> Enum.map(&String.to_integer/1)
    |> run(args)
    |> Integer.to_string()
    |> Mix.shell().info()
  end

  defp run(input, []), do: PartOne.run(input)
  defp run(input, _), do: PartTwo.run(input)
end
