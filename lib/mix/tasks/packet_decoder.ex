defmodule Mix.Tasks.PacketDecoder do
  use Mix.Task

  @shortdoc "Day 16"

  @moduledoc """
  Given the input file priv/input_16.txt find the solution

  ## Example

  mix packet_decoder (--part2)
  """

  alias PacketDecoder.{PartOne, PartTwo}

  def run(args) do
    "priv/input_16.txt"
    |> File.read!()
    |> String.trim()
    |> run(args)
    |> Integer.to_string()
    |> Mix.shell().info()
  end

  defp run(input, []), do: PartOne.run(input)
  defp run(input, _), do: PartTwo.run(input)
end
