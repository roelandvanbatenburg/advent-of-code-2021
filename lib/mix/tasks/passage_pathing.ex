defmodule Mix.Tasks.PassagePathing do
  use Mix.Task

  @shortdoc "Day 12"

  @moduledoc """
  Given the input file priv/input_12.txt solve the riddle

  ## Example

  mix passage_pathing (--part2)
  """

  alias PassagePathing.{PartOne, PartTwo}

  def run(args) do
    "priv/input_12.txt"
    |> File.stream!()
    |> Stream.map(&String.trim_trailing/1)
    |> Enum.to_list()
    |> run(args)
    |> Integer.to_string()
    |> Mix.shell().info()
  end

  defp run(input, []), do: PartOne.run(input)
  defp run(input, _), do: PartTwo.run(input)
end
