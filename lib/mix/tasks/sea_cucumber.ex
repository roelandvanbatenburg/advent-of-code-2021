defmodule Mix.Tasks.SeaCucumber do
  use Mix.Task

  @shortdoc "Day 25"

  @moduledoc """
  Given the input file priv/input_25.txt reach the seafloor

  ## Example

  mix sea_cucumber
  """

  alias SeaCucumber.PartOne

  def run(args) do
    "priv/input_25.txt"
    |> File.stream!()
    |> Enum.to_list()
    |> run(args)
    |> Integer.to_string()
    |> Mix.shell().info()
  end

  defp run(input, []), do: PartOne.run(input)
end
