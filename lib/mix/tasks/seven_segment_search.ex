defmodule Mix.Tasks.SevenSegmentSearch do
  use Mix.Task

  @shortdoc "Day 08"

  @moduledoc """
  Given the input file priv/input_08.txt solve the riddle

  ## Example

  mix seven_segment_search (--part2)
  """

  alias SevenSegmentSearch.{PartOne, PartTwo}

  def run(args) do
    "priv/input_08.txt"
    |> File.stream!()
    |> Stream.map(&String.trim_trailing/1)
    |> Enum.to_list()
    |> run(args)
    |> Integer.to_string()
    |> Mix.shell().info()
  end

  defp run(input, []), do: PartOne.run(input)
  defp run(input, _), do: PartTwo.run(input)
end
