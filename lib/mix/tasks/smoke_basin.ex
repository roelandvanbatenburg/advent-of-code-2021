defmodule Mix.Tasks.SmokeBasin do
  use Mix.Task

  @shortdoc "Day 09"

  @moduledoc """
  Given the input file priv/input_09.txt find a safe route

  ## Example

  mix smoke_basin (--part2)
  """

  alias SmokeBasin.{PartOne, PartTwo}

  def run(args) do
    "priv/input_09.txt"
    |> File.stream!()
    |> Stream.map(&String.trim_trailing/1)
    |> Enum.to_list()
    |> run(args)
    |> Integer.to_string()
    |> Mix.shell().info()
  end

  defp run(input, []), do: PartOne.run(input)
  defp run(input, _), do: PartTwo.run(input)
end
