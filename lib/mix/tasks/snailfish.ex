defmodule Mix.Tasks.Snailfish do
  use Mix.Task

  @shortdoc "Day 18"

  @moduledoc """
  Given the input file priv/input_18.txt solve the problem

  ## Example

  mix snailfish (--part2)
  """

  alias Snailfish.{PartOne, PartTwo}

  def run(args) do
    "priv/input_18.txt"
    |> File.stream!()
    |> Enum.to_list()
    |> run(args)
    |> Integer.to_string()
    |> Mix.shell().info()
  end

  defp run(input, []), do: PartOne.run(input)
  defp run(input, _), do: PartTwo.run(input)
end
