defmodule Mix.Tasks.SonarSweep do
  use Mix.Task

  @shortdoc "Day 01"

  @moduledoc """
  Given the input file priv/input_01.txt find the keys to the sleigh

  ## Example

  mix sonar_sweep (--part2)
  """

  def run(args) do
    input =
      File.stream!("priv/input_01.txt")
      |> Stream.map(&String.trim_trailing/1)
      |> Stream.map(&String.to_integer/1)
      |> Enum.to_list()

    if args == [] do
      SonarSweep.run_1(input)
    else
      SonarSweep.run_2(input)
    end
    |> Integer.to_string()
    |> Mix.shell().info()
  end
end
