defmodule Mix.Tasks.SyntaxScoring do
  use Mix.Task

  @shortdoc "Day 03"

  @moduledoc """
  Given the input file priv/input_10.txt diagnoze the problem

  ## Example

  mix syntax_scoring (--part2)
  """

  alias SyntaxScoring.{PartOne, PartTwo}

  def run(args) do
    "priv/input_10.txt"
    |> File.stream!()
    |> Enum.to_list()
    |> run(args)
    |> Integer.to_string()
    |> Mix.shell().info()
  end

  defp run(input, []), do: PartOne.run(input)
  defp run(input, _), do: PartTwo.run(input)
end
