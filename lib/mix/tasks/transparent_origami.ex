defmodule Mix.Tasks.TransparentOrigami do
  use Mix.Task

  @shortdoc "Day 13"

  @moduledoc """
  Given the input file priv/input_13.txt solve the riddle

  ## Example

  mix transparent_origami (--part2)
  """

  alias TransparentOrigami.{PartOne, PartTwo}

  def run(args) do
    "priv/input_13.txt"
    |> File.stream!()
    |> Enum.join("")
    |> run(args)
    |> Integer.to_string()
    |> Mix.shell().info()
  end

  defp run(input, []), do: PartOne.run(input)
  defp run(input, _), do: PartTwo.run(input)
end
