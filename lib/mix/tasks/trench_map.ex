defmodule Mix.Tasks.TrenchMap do
  use Mix.Task

  @shortdoc "Day 20"

  @moduledoc """
  Given the input file priv/input_20.txt decode the image

  ## Example

  mix trench_map (--part2)
  """

  alias TrenchMap.{PartOne, PartTwo}

  def run(args) do
    "priv/input_20.txt"
    |> File.stream!()
    |> Stream.map(&String.trim_trailing/1)
    |> Enum.to_list()
    |> run(args)
    |> Integer.to_string()
    |> Mix.shell().info()
  end

  defp run(input, []), do: PartOne.run(input)
  defp run(input, _), do: PartTwo.run(input)
end
