defmodule Mix.Tasks.TrickShot do
  use Mix.Task

  @shortdoc "Day 17"

  @moduledoc """
  Given the input file priv/input_17.txt find the solution

  ## Example

  mix trick_shot (--part2)
  """

  alias TrickShot.{PartOne, PartTwo}

  def run(args) do
    "priv/input_17.txt"
    |> File.read!()
    |> String.trim()
    |> run(args)
    |> Integer.to_string()
    |> Mix.shell().info()
  end

  defp run(input, []), do: PartOne.run(input)
  defp run(input, _), do: PartTwo.run(input)
end
