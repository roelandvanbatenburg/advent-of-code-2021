defmodule Mix.Tasks.Whales do
  use Mix.Task

  @shortdoc "Day 07"

  @moduledoc """
  Given the input file priv/input_07.txt escape the whales

  ## Example

  mix whales (--part2)
  """

  alias Whales.{PartOne, PartTwo}

  def run(args) do
    "priv/input_07.txt"
    |> File.read!()
    |> String.trim()
    |> String.split(",")
    |> Enum.map(&String.to_integer/1)
    |> run(args)
    |> Integer.to_string()
    |> Mix.shell().info()
  end

  defp run(input, []), do: PartOne.run(input)
  defp run(input, _), do: PartTwo.run(input)
end
