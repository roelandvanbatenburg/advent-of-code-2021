defmodule PacketDecoder do
  @moduledoc """
  Read the packet
  """

  defmodule PartOne do
    @moduledoc """
    First puzzle
    """

    @spec run(String.t()) :: integer()
    def run(input) do
      input
      |> PacketDecoder.parse()
      |> read_packet(0)
      |> elem(0)
    end

    defp read_packet(bits, index) do
      if index > String.length(bits) do
        :done
      else
        version = String.slice(bits, index, 3) |> String.to_integer(2)
        type = String.slice(bits, index + 3, 3) |> String.to_integer(2)

        if type == 4 do
          {version, read_literal(bits, index + 6)}
        else
          read_subpackets(bits, index, version)
        end
      end
    end

    defp read_literal(bits, index) do
      case String.at(bits, index) do
        "1" -> read_literal(bits, index + 5)
        "0" -> index + 5
      end
    end

    defp read_subpackets(bits, index, acc) do
      if index + 5 >= String.length(bits) do
        :done
      else
        case String.at(bits, index + 6) do
          "0" -> read_subpackets_by_length(bits, index, acc)
          "1" -> read_subpackets_by_count(bits, index, acc)
        end
      end
    end

    defp read_subpackets_by_length(bits, index, acc) do
      length = String.slice(bits, index + 7, 15) |> String.to_integer(2)
      read_subpackets_till_length(bits, index + 7 + 15, length, acc)
    end

    defp read_subpackets_till_length(_bits, index, 0, acc), do: {acc, index}

    defp read_subpackets_till_length(bits, index, length, acc) do
      case read_packet(bits, index) do
        :done ->
          {acc, :done}

        {version, :done} ->
          {version + acc, :done}

        {version, next_index} ->
          read_subpackets_till_length(
            bits,
            next_index,
            length - (next_index - index),
            version + acc
          )
      end
    end

    defp read_subpackets_by_count(bits, index, acc) do
      count = String.slice(bits, index + 7, 11) |> String.to_integer(2)
      read_subpackets_till_count(bits, index + 7 + 11, count, acc)
    end

    defp read_subpackets_till_count(_bits, index, 0, acc), do: {acc, index}

    defp read_subpackets_till_count(bits, index, count, acc) do
      case read_packet(bits, index) do
        :done ->
          {acc, :done}

        {version, :done} ->
          {version + acc, :done}

        {version, next_index} ->
          read_subpackets_till_count(bits, next_index, count - 1, acc + version)
      end
    end
  end

  defmodule PartTwo do
    @moduledoc """
    Second puzzle
    """

    @spec run(String.t()) :: integer()
    def run(input) do
      input
      |> PacketDecoder.parse()
      |> read_packet(0)
      |> elem(1)
    end

    defp read_packet(bits, index) do
      if index > String.length(bits) do
        :done
      else
        type = String.slice(bits, index + 3, 3) |> String.to_integer(2)

        if type == 4 do
          read_literal(bits, index + 6)
        else
          read_subpackets(bits, index, type)
        end
      end
    end

    defp read_literal(bits, index, acc \\ "") do
      case String.at(bits, index) do
        "1" -> read_literal(bits, index + 5, acc <> String.slice(bits, index + 1, 4))
        "0" -> {index + 5, String.to_integer(acc <> String.slice(bits, index + 1, 4), 2)}
      end
    end

    defp read_subpackets(bits, index, type) do
      if index + 5 >= String.length(bits) do
        :done
      else
        {next, values} =
          case String.at(bits, index + 6) do
            "0" -> read_subpackets_by_length(bits, index)
            "1" -> read_subpackets_by_count(bits, index)
          end

        {next, calculate(type, values)}
      end
    end

    defp read_subpackets_by_length(bits, index) do
      length = String.slice(bits, index + 7, 15) |> String.to_integer(2)
      read_subpackets_till_length(bits, index + 7 + 15, length, [])
    end

    defp read_subpackets_till_length(_bits, next, 0, acc), do: {next, acc}

    defp read_subpackets_till_length(bits, index, length, acc) do
      case read_packet(bits, index) do
        :done ->
          {:done, acc}

        {:done, value} ->
          {:done, [value | acc]}

        {next, value} ->
          read_subpackets_till_length(bits, next, length - (next - index), [value | acc])
      end
    end

    defp read_subpackets_by_count(bits, index) do
      count = String.slice(bits, index + 7, 11) |> String.to_integer(2)
      read_subpackets_till_count(bits, index + 7 + 11, count, [])
    end

    defp read_subpackets_till_count(_bits, next, 0, acc), do: {next, acc}

    defp read_subpackets_till_count(bits, index, count, acc) do
      case read_packet(bits, index) do
        :done -> {:done, acc}
        {:done, value} -> {:done, [value | acc]}
        {next, value} -> read_subpackets_till_count(bits, next, count - 1, [value | acc])
      end
    end

    defp calculate(0, values), do: Enum.sum(values)
    defp calculate(1, values), do: Enum.product(values)
    defp calculate(2, values), do: Enum.min(values)
    defp calculate(3, values), do: Enum.max(values)
    defp calculate(5, [two, one]) when one > two, do: 1
    defp calculate(5, _), do: 0
    defp calculate(6, [two, one]) when one < two, do: 1
    defp calculate(6, _), do: 0
    defp calculate(7, [one, one]), do: 1
    defp calculate(7, _), do: 0
  end

  @spec parse(String.t()) :: any()
  def parse(input) do
    input
    |> String.to_integer(16)
    |> Integer.to_string(2)
    |> pad()
    |> add_zeroes(input)
  end

  defp pad(bits) do
    l = String.length(bits)

    padding =
      case rem(l, 4) do
        0 -> 0
        r -> 4 - r
      end

    String.pad_leading(bits, l + padding, "0")
  end

  defp add_zeroes(bits, "0" <> _str), do: "0000" <> bits
  defp add_zeroes(bits, _), do: bits
end
