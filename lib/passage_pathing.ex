defmodule PassagePathing do
  @moduledoc """
  Decode the display
  """

  defmodule PartOne do
    @moduledoc """
    First puzzle
    """

    @spec run([String.t()]) :: integer()
    def run(input) do
      input
      |> PassagePathing.parse()
      |> PassagePathing.step([], "start", [], &can_visit?/2)
      |> length()
    end

    defp can_visit?(_route, "start"), do: false

    defp can_visit?(route, cave) do
      if String.upcase(cave) == cave do
        true
      else
        not Enum.member?(route, cave)
      end
    end
  end

  defmodule PartTwo do
    @moduledoc """
    Second puzzle
    """

    @spec run([String.t()]) :: integer()
    def run(input) do
      input
      |> PassagePathing.parse()
      |> PassagePathing.step([], "start", [], &can_visit?/2)
      |> length()
    end

    defp can_visit?(_route, "start"), do: false

    defp can_visit?(route, cave) do
      if String.upcase(cave) == cave do
        true
      else
        if Enum.member?(route, cave) do
          if cave == "end" do
            false
          else
            # only allowed if there are no other small caves that have been visited twice
            small_caves = Enum.filter(route, fn cave -> String.downcase(cave) == cave end)
            length(small_caves) == length(Enum.uniq(small_caves))
          end
        else
          true
        end
      end
    end
  end

  @type cave :: String.t()
  @type connections :: %{cave() => [cave()]}
  @type route :: [cave()]
  @type can_visit_fn :: (route, cave -> boolean())

  @spec parse([String.t()]) :: connections()
  def parse(input) do
    input
    |> Enum.reduce(%{}, fn line, acc ->
      [c1, c2] = String.split(line, "-")

      acc
      |> Map.update(c1, [c2], &[c2 | &1])
      |> Map.update(c2, [c1], &[c1 | &1])
    end)
  end

  @spec step(connections(), route(), cave(), [route], can_visit_fn()) :: [route]
  def step(_connections, route, "end", paths, _can_visit_fn) do
    [["end" | route] | paths]
  end

  def step(connections, route, current, paths, can_visit_fn) do
    find_next_caves(connections, current, route, can_visit_fn)
    |> Enum.flat_map(&step(connections, [current | route], &1, paths, can_visit_fn))
  end

  defp find_next_caves(connections, current, route, can_visit_fn) do
    connections
    |> Map.fetch!(current)
    |> Enum.filter(&can_visit_fn.([current | route], &1))
  end
end
