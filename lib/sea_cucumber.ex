defmodule SeaCucumber do
  @moduledoc """
  Move the sea cubumbers
  """

  @type cucumber_map :: {cucumbers(), dimension()}
  @type cucumbers :: %{{integer(), integer()} => :east | :south}
  @type dimension :: {integer(), integer()}

  defmodule PartOne do
    @moduledoc """
    First puzzle
    """

    @spec run([String.t()]) :: integer()
    def run(input) do
      input
      |> SeaCucumber.parse()
      |> step()
    end

    @spec step(SeaCucumber.cucumber_map(), integer()) :: integer()
    def step({cucumbers, dimensions}, steps \\ 1) do
      east_candidates = candidates(cucumbers, dimensions, :east)
      cucumbers = move(cucumbers, dimensions, east_candidates, :east)
      south_candidates = candidates(cucumbers, dimensions, :south)
      cucumbers = move(cucumbers, dimensions, south_candidates, :south)

      moved_cucumbers = length(east_candidates) + length(south_candidates)

      if moved_cucumbers > 0 do
        step({cucumbers, dimensions}, steps + 1)
      else
        steps
      end
    end

    defp candidates(cucumbers, dimension, direction) do
      cucumbers
      |> Stream.filter(&(elem(&1, 1) == direction))
      |> Stream.filter(&can_move?(cucumbers, &1, dimension))
      |> Enum.map(&elem(&1, 0))
    end

    defp can_move?(cucumbers, {position, direction}, dimensions) do
      target = target(position, dimensions, direction)
      not Map.has_key?(cucumbers, target)
    end

    defp move(cucumbers, dimensions, candidates, direction) do
      cucumbers
      |> Map.drop(candidates)
      |> add(candidates, dimensions, direction)
    end

    defp add(cucumbers, candidates, dimensions, direction) do
      candidates
      |> Enum.reduce(cucumbers, fn position, acc ->
        Map.put(acc, target(position, dimensions, direction), direction)
      end)
    end

    defp target({y, x}, {_max_y, max_x}, :east), do: {y, axis(x, max_x)}
    defp target({y, x}, {max_y, _max_x}, :south), do: {axis(y, max_y), x}

    defp axis(v, v), do: 0
    defp axis(v, _max_v), do: v + 1
  end

  @spec parse([String.t()]) :: cucumber_map()
  def parse(input) do
    max_y = length(input) - 1
    max_x = Enum.at(input, 0) |> String.trim() |> String.length() |> Kernel.-(1)

    cucumbers =
      0..max_y
      |> Enum.reduce(%{}, fn y, acc ->
        line = String.trim(Enum.at(input, y))

        0..max_x
        |> Enum.reduce(acc, &parse_position(&2, y, &1, line))
      end)

    {cucumbers, {max_y, max_x}}
  end

  defp parse_position(cucumbers, y, x, line) do
    case String.at(line, x) do
      "." -> cucumbers
      ">" -> Map.put(cucumbers, {y, x}, :east)
      "v" -> Map.put(cucumbers, {y, x}, :south)
    end
  end
end
