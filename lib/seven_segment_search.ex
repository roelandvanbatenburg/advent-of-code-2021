defmodule SevenSegmentSearch do
  @moduledoc """
  Decode the display
  """

  defmodule PartOne do
    @moduledoc """
    First puzzle
    """

    @spec run([String.t()]) :: integer()
    def run(input) do
      input
      |> SevenSegmentSearch.parse()
      |> Enum.reduce(0, fn {_signal, output}, acc -> acc + count_unique_digits(output) end)
    end

    defp count_unique_digits(output) do
      output
      |> Enum.map(&String.length/1)
      |> Enum.filter(&Enum.member?([2, 3, 4, 7], &1))
      |> length()
    end
  end

  defmodule PartTwo do
    @moduledoc """
    Second puzzle
    """

    @spec run([String.t()]) :: integer()
    def run(input) do
      input
      |> SevenSegmentSearch.parse()
      |> Enum.reduce(0, fn {signal, output}, acc ->
        signal
        |> signal_map()
        |> decode(output)
        |> Kernel.+(acc)
      end)
    end

    defp signal_map(signal) do
      groups =
        signal
        |> Stream.map(&String.graphemes/1)
        |> Stream.map(&MapSet.new/1)
        |> Enum.group_by(&MapSet.size/1)

      one = Map.get(groups, 2) |> List.first()
      seven = Map.get(groups, 3) |> List.first()
      four = Map.get(groups, 4) |> List.first()
      two_three_five = Map.get(groups, 5)
      zero_six_nine = Map.get(groups, 6)
      eight = Map.get(groups, 7) |> List.first()
      # calculate top directly from seven and one
      t = MapSet.difference(seven, one) |> unpack()

      # calculate middle, left top and left bottom from two_three_five, four and one
      m_and_lt = MapSet.difference(four, one)

      lt_and_lb =
        two_three_five
        |> Enum.flat_map(&MapSet.to_list/1)
        |> Enum.frequencies()
        |> Enum.filter(fn {_, count} -> count == 1 end)
        |> Enum.map(&elem(&1, 0))
        |> MapSet.new()

      lt_set = MapSet.intersection(m_and_lt, lt_and_lb)
      lt = unpack(lt_set)
      m_set = MapSet.difference(m_and_lt, lt_set)
      m = unpack(m_set)
      lb_set = MapSet.difference(lt_and_lb, lt_set)
      lb = unpack(lb_set)

      # calculate bottom and right top from zero_six_nine and middle
      b_and_lb = MapSet.difference(MapSet.difference(eight, seven), four)

      rt_and_lb =
        zero_six_nine
        |> Enum.flat_map(&MapSet.to_list/1)
        |> Enum.frequencies()
        |> Enum.filter(fn {_, count} -> count == 2 end)
        |> Enum.map(&elem(&1, 0))
        |> MapSet.new()
        |> MapSet.difference(m_set)

      b = MapSet.difference(b_and_lb, lb_set) |> unpack()
      rt_set = MapSet.difference(rt_and_lb, lb_set)
      rt = unpack(rt_set)

      # calculate rb from one and rt
      rb = MapSet.difference(one, rt_set) |> unpack()

      zero = MapSet.new([t, lt, rt, lb, rb, b])
      two = MapSet.new([t, rt, m, lb, b])
      three = MapSet.new([t, rt, m, rb, b])
      five = MapSet.new([t, lt, m, rb, b])
      six = MapSet.new([t, lt, m, lb, rb, b])
      nine = MapSet.new([t, lt, rt, m, rb, b])

      %{
        one => "1",
        two => "2",
        three => "3",
        four => "4",
        five => "5",
        six => "6",
        seven => "7",
        eight => "8",
        nine => "9",
        zero => "0"
      }
    end

    defp unpack(mapset) do
      mapset
      |> MapSet.to_list()
      |> List.first()
    end

    defp decode(signal_map, output) do
      output
      |> Enum.map(&decode_digits(signal_map, &1))
      |> Enum.join()
      |> String.to_integer()
    end

    defp decode_digits(signal_map, digits) do
      key =
        digits
        |> String.graphemes()
        |> Enum.sort()
        |> MapSet.new()

      Map.fetch!(signal_map, key)
    end
  end

  @spec parse([String.t()]) :: [{[String.t()], [String.t()]}]
  def parse(input) do
    input
    |> Enum.map(fn line ->
      [signal, output] = String.split(line, " | ")
      {String.split(signal), String.split(output)}
    end)
  end
end
