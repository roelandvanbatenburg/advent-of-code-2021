defmodule SmokeBasin do
  @moduledoc """
  Find a safe route
  """

  defmodule PartOne do
    @moduledoc """
    First puzzle
    """

    @spec run([String.t()]) :: integer()
    def run(input) do
      input
      |> SmokeBasin.parse()
      |> SmokeBasin.low_points()
      |> Enum.reduce(0, &(elem(&1, 1) + 1 + &2))
    end
  end

  defmodule PartTwo do
    @moduledoc """
    Second puzzle
    """

    @spec run([String.t()]) :: integer()
    def run(input) do
      points = SmokeBasin.parse(input)

      points
      |> SmokeBasin.low_points()
      |> Stream.map(&basin(&1, points))
      |> Stream.map(&Enum.uniq/1)
      |> Stream.map(&length/1)
      |> Enum.sort(:desc)
      |> Stream.take(3)
      |> Enum.reduce(&Kernel.*(&1, &2))
    end

    defp basin({{y, x}, height} = point, points) do
      [
        point,
        add_basin_points(height, get_point(points, y - 1, x), points),
        add_basin_points(height, get_point(points, y + 1, x), points),
        add_basin_points(height, get_point(points, y, x + 1), points),
        add_basin_points(height, get_point(points, y, x - 1), points)
      ]
      |> List.flatten()
    end

    defp get_point(points, y, x), do: {{y, x}, Map.get(points, {y, x})}

    defp add_basin_points(_h1, {_position, nil}, _points), do: []
    defp add_basin_points(_h1, {_position, 9}, _points), do: []
    defp add_basin_points(h1, {_position, h2}, _points) when h1 >= h2, do: []
    defp add_basin_points(_h1, point, points), do: basin(point, points)
  end

  @spec parse([String.t()]) :: %{{integer(), integer()} => integer()}
  def parse(input) do
    0..(length(input) - 1)
    |> Enum.reduce(%{}, fn y, acc ->
      line = Enum.at(input, y)

      0..(String.length(line) - 1)
      |> Enum.reduce(acc, fn x, acc ->
        Map.put(acc, {y, x}, String.to_integer(String.at(line, x)))
      end)
    end)
  end

  @spec low_points(%{{integer(), integer()} => integer()}) ::
          [{{integer(), integer()}, integer()}]
  def low_points(points), do: Enum.filter(points, &low_point?(&1, points))

  defp low_point?({{y, x}, height}, points) do
    lower?(height, Map.get(points, {y - 1, x})) and
      lower?(height, Map.get(points, {y + 1, x})) and
      lower?(height, Map.get(points, {y, x + 1})) and
      lower?(height, Map.get(points, {y, x - 1}))
  end

  defp lower?(_height, nil), do: true
  defp lower?(height, reference_height), do: height < reference_height
end
