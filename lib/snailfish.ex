defmodule Snailfish do
  @moduledoc """
  Do the math
  """

  defmodule Number do
    @moduledoc """
    A snailfish number consist of two pairs
    """

    @enforce_keys [:x, :y]
    defstruct @enforce_keys

    @type value :: t() | integer()
    @type t() :: %__MODULE__{x: value(), y: value}
    @type path :: [:x | :y]

    @spec parse(String.t()) :: t()
    def parse(input) do
      {[x, y], []} = Code.eval_string(input)
      %__MODULE__{x: parse_part(x), y: parse_part(y)}
    end

    defp parse_part(n) when is_integer(n), do: n
    defp parse_part([x, y]), do: %__MODULE__{x: parse_part(x), y: parse_part(y)}

    @spec add(t(), t()) :: t()
    def add(x, y), do: %__MODULE__{x: x, y: y}

    defp update(_n, [], replacement, :replace), do: replacement
    defp update(n, [], replacement, :add), do: n + replacement

    defp update(n, [key | path], replacement, opp),
      do: Map.put(n, key, update(Map.get(n, key), path, replacement, opp))

    defp get(%__MODULE__{} = n, [key]), do: Map.get(n, key)
    defp get(%__MODULE__{} = n, [key | path]), do: get(Map.get(n, key), path)
    defp get(_, _), do: nil

    @spec reduce(t()) :: t()
    def reduce(n), do: explode(n, true)

    defp explode(n, split?) do
      case path_to_explosion_target(n) do
        false -> split(n, not split?)
        target -> do_explode(n, target)
      end
    end

    @spec path_to_explosion_target(t(), integer(), path()) :: path() | false
    def path_to_explosion_target(%__MODULE__{x: x, y: y}, depth \\ 0, acc \\ []) do
      case {depth >= 4, is_integer(x), is_integer(y)} do
        {true, true, true} -> Enum.reverse(acc)
        {false, true, true} -> false
        {_, false, true} -> path_to_explosion_target(x, depth + 1, [:x | acc])
        {_, true, false} -> path_to_explosion_target(y, depth + 1, [:y | acc])
        {_, false, false} -> both_explosion_paths(x, y, depth, acc)
      end
    end

    defp both_explosion_paths(x, y, depth, acc) do
      case path_to_explosion_target(x, depth + 1, [:x | acc]) do
        false -> path_to_explosion_target(y, depth + 1, [:y | acc])
        target -> target
      end
    end

    def do_explode(n, path) do
      n
      |> update_neighbour(path, :x)
      |> update_neighbour(path, :y)
      |> update(path, 0, :replace)
      |> reduce()
    end

    defp update_neighbour(n, path, key) do
      case find_neighbour(n, path, key) do
        nil -> n
        path_to_neighbour -> update(n, path_to_neighbour, Map.get(get(n, path), key), :add)
      end
    end

    @spec find_neighbour(t(), path(), :x | :y) :: path() | nil
    defp find_neighbour(_n, [], _), do: nil
    defp find_neighbour(n, [:y], :x), do: dive(n, [:x], :y)
    defp find_neighbour(n, [:x], :y), do: dive(n, [:y], :x)

    defp find_neighbour(n, path, target) do
      {current, path_remainder} = List.pop_at(path, -1)

      if current == target do
        find_neighbour(n, path_remainder, target)
      else
        # reverse to keep to this side
        dive(n, path_remainder ++ [target], reverse(target))
      end
    end

    defp dive(n, path, target) do
      case get(n, path) do
        nil -> nil
        v when is_integer(v) -> path
        _ -> dive(n, path ++ [target], target)
      end
    end

    defp reverse(:x), do: :y
    defp reverse(:y), do: :x

    defp split(n, exploded?) do
      case path_to_split(n) do
        false -> maybe_done(n, exploded?)
        target -> do_split(n, target)
      end
    end

    defp path_to_split(%__MODULE__{x: x, y: y}, acc \\ []) do
      case {is_integer(x), is_integer(y), x >= 10, y >= 10} do
        {true, _, true, _} -> Enum.reverse([:x | acc])
        {false, true, _, true} -> path_to_split_or_y(x, acc)
        {false, true, _, _} -> path_to_split(x, [:x | acc])
        {true, true, _, true} -> Enum.reverse([:y | acc])
        {true, true, _, _} -> false
        {true, false, _, _} -> path_to_split(y, [:y | acc])
        {false, false, _, _} -> path_to_split_both(x, y, acc)
      end
    end

    defp path_to_split_or_y(x, acc) do
      target = path_to_split(x, [:x | acc])

      if target do
        target
      else
        Enum.reverse([:y | acc])
      end
    end

    defp path_to_split_both(x, y, acc) do
      target = path_to_split(x, [:x | acc])

      if target do
        target
      else
        path_to_split(y, [:y | acc])
      end
    end

    defp maybe_done(n, false), do: n

    defp do_split(n, path) do
      v = get(n, path)
      number = %__MODULE__{x: Integer.floor_div(v, 2), y: ceil(v / 2)}

      n
      |> update(path, number, :replace)
      |> reduce()
    end

    @spec magnitude(t()) :: integer
    def magnitude(n) when is_integer(n), do: n
    def magnitude(%__MODULE__{x: x, y: y}), do: 3 * magnitude(x) + 2 * magnitude(y)
  end

  defmodule PartOne do
    @moduledoc """
    First puzzle
    """

    @spec run([String.t()]) :: integer()
    def run(input) do
      input
      |> Snailfish.parse()
      |> Enum.reduce(fn n, acc ->
        acc
        |> Number.add(n)
        |> Number.reduce()
      end)
      |> Number.magnitude()
    end
  end

  defmodule PartTwo do
    @moduledoc """
    Second puzzle
    """

    @spec run([String.t()]) :: integer()
    def run(input) do
      numbers = Snailfish.parse(input)
      number_of_numbers = length(numbers)

      for(
        y <- 0..(number_of_numbers - 1),
        x <- 0..(number_of_numbers - 1),
        do: calculate_magnitude(numbers, x, y)
      )
      |> Enum.max()
    end

    defp calculate_magnitude(_numbers, i, i), do: 0

    defp calculate_magnitude(numbers, i, j) do
      Number.add(Enum.at(numbers, i), Enum.at(numbers, j))
      |> Number.reduce()
      |> Number.magnitude()
    end
  end

  @spec parse([String.t()]) :: [Number.t()]
  def parse(input), do: Enum.map(input, &Number.parse/1)
end
