defmodule SonarSweep do
  @moduledoc """
  Find the keys
  """

  @spec run_1([integer()]) :: integer()
  def run_1(input) do
    {count, _last} = Enum.reduce(input, {0, nil}, &update/2)
    count
  end

  defp update(current, {count, previous}) when is_nil(previous), do: {count, current}
  defp update(current, {count, previous}) when current > previous, do: {count + 1, current}
  defp update(current, {count, _previous}), do: {count, current}

  @spec run_2([integer()]) :: integer()
  def run_2(input) do
    {count, _last} =
      input
      |> Enum.chunk_every(3, 1, :discard)
      |> Enum.reduce({0, nil}, &update_with_sum/2)

    count
  end

  defp update_with_sum(current, {count, previous}) when is_nil(previous) do
    {count, Enum.sum(current)}
  end

  defp update_with_sum(current, {count, previous}) do
    current_sum = Enum.sum(current)

    if current_sum > previous do
      {count + 1, current_sum}
    else
      {count, current_sum}
    end
  end
end
