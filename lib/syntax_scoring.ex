defmodule SyntaxScoring do
  @moduledoc """
  Fix the syntax?
  """

  defmodule PartOne do
    @moduledoc """
    First puzzle
    """

    @spec run([String.t()]) :: integer()
    def run(input) do
      input
      |> Enum.map(&parse_line/1)
      |> Enum.reduce(0, &score/2)
    end

    defp parse_line(line) do
      line
      |> String.graphemes()
      |> Enum.reduce_while([], &handle_token/2)
    end

    # no errors detected, just incomplete
    defp handle_token("\n", _), do: {:halt, :incomplete}
    # valid closing of current chunk
    defp handle_token("]", ["[" | previous]), do: {:cont, previous}
    defp handle_token(")", ["(" | previous]), do: {:cont, previous}
    defp handle_token(">", ["<" | previous]), do: {:cont, previous}
    defp handle_token("}", ["{" | previous]), do: {:cont, previous}
    # corrupted chunk
    defp handle_token("]", _), do: {:halt, "]"}
    defp handle_token(")", _), do: {:halt, ")"}
    defp handle_token(">", _), do: {:halt, ">"}
    defp handle_token("}", _), do: {:halt, "}"}
    # opening new chunk
    defp handle_token(open_bracket, previous), do: {:cont, [open_bracket | previous]}

    defp score(:incomplete, score), do: score
    defp score(")", score), do: score + 3
    defp score("]", score), do: score + 57
    defp score("}", score), do: score + 1197
    defp score(">", score), do: score + 25_137
  end

  defmodule PartTwo do
    @moduledoc """
    Second puzzle
    """

    @spec run([String.t()]) :: integer()
    def run(input) do
      input
      |> Stream.map(&parse_line/1)
      |> Stream.reject(&Kernel.==(&1, :corrupted))
      |> Stream.map(&score/1)
      |> Enum.sort()
      |> middle()
    end

    defp parse_line(line) do
      line
      |> String.graphemes()
      |> Enum.reduce_while([], &handle_token/2)
    end

    # return open tokens
    defp handle_token("\n", acc), do: {:halt, acc}
    # valid closing of current chunk
    defp handle_token("]", ["[" | previous]), do: {:cont, previous}
    defp handle_token(")", ["(" | previous]), do: {:cont, previous}
    defp handle_token(">", ["<" | previous]), do: {:cont, previous}
    defp handle_token("}", ["{" | previous]), do: {:cont, previous}
    # corrupted chunk
    defp handle_token("]", _), do: {:halt, :corrupted}
    defp handle_token(")", _), do: {:halt, :corrupted}
    defp handle_token(">", _), do: {:halt, :corrupted}
    defp handle_token("}", _), do: {:halt, :corrupted}
    # opening new chunk
    defp handle_token(open_bracket, previous), do: {:cont, [open_bracket | previous]}

    defp score(open_chunks) do
      open_chunks
      |> Enum.reduce(0, fn token, acc ->
        5 * acc + value(token)
      end)
    end

    defp value("("), do: 1
    defp value("["), do: 2
    defp value("{"), do: 3
    defp value("<"), do: 4

    defp middle(scores) do
      scores = Enum.sort(scores)
      middle_index = div(length(scores) - 1, 2)
      Enum.at(scores, middle_index)
    end
  end
end
