defmodule TransparentOrigami do
  @moduledoc """
  Fold the paper
  """

  @type dots :: MapSet.t(dot())
  @type dot :: {integer(), integer()}
  @type folds :: [fold()]
  @type fold :: {:y | :x, integer()}

  defmodule PartOne do
    @moduledoc """
    First puzzle
    """

    @spec run(String.t()) :: integer()
    def run(input) do
      input
      |> TransparentOrigami.parse()
      |> fold()
      |> elem(0)
      |> MapSet.size()
    end

    defp fold({dots, [fold | folds]}) do
      dots =
        dots
        |> Enum.map(&TransparentOrigami.transpose(&1, fold))
        |> MapSet.new()

      {dots, folds}
    end
  end

  defmodule PartTwo do
    @moduledoc """
    Second puzzle
    """

    @spec run(String.t()) :: integer()
    def run(input) do
      input
      |> TransparentOrigami.parse()
      |> fold()
      |> print()
      |> MapSet.size()
    end

    defp fold({dots, []}), do: dots

    defp fold({dots, [fold | folds]}) do
      dots =
        dots
        |> Enum.map(&TransparentOrigami.transpose(&1, fold))
        |> MapSet.new()

      fold({dots, folds})
    end

    defp print(dots) do
      row_size = Enum.max_by(dots, &elem(&1, 1)) |> elem(1)
      column_size = Enum.max_by(dots, &elem(&1, 0)) |> elem(0)

      for row <- 0..row_size do
        for col <- 0..column_size do
          "" <> symbol(MapSet.member?(dots, {col, row}))
        end
        |> Enum.join(".")
      end
      |> Enum.join("\n")
      |> IO.puts()

      IO.puts("\n")

      dots
    end

    defp symbol(true), do: "#"
    defp symbol(false), do: "."
  end

  @spec parse(String.t()) :: {dots(), folds()}
  def parse(input) do
    [dots, folds] = String.split(input, "\n\n")
    {parse_dots(dots), parse_folds(folds)}
  end

  defp parse_dots(dots) do
    dots
    |> String.split()
    |> Enum.reduce(MapSet.new(), fn dot, acc ->
      [x, y] = String.split(dot, ",")
      MapSet.put(acc, {String.to_integer(x), String.to_integer(y)})
    end)
  end

  defp parse_folds(folds) do
    folds
    |> String.trim()
    |> String.split("\n")
    |> Enum.reduce([], fn fold, acc ->
      [parse_fold(fold) | acc]
    end)
    |> Enum.reverse()
  end

  defp parse_fold("fold along x=" <> column), do: {:x, String.to_integer(column)}
  defp parse_fold("fold along y=" <> row), do: {:y, String.to_integer(row)}

  @spec transpose(dot(), fold) :: dot()
  def transpose({x, y}, {:x, column}) when x < column, do: {x, y}
  def transpose({x, y}, {:x, column}), do: {2 * column - x, y}

  def transpose({x, y}, {:y, row}) when y < row, do: {x, y}
  def transpose({x, y}, {:y, row}), do: {x, 2 * row - y}
end
