defmodule TrenchMap do
  @moduledoc """
  Decode the display
  """

  @type pixel :: :dark | :light
  @type location :: {integer(), integer()}
  @type trench_map :: %{location() => pixel()}
  @type algorithm :: %{integer() => pixel()}

  defmodule PartOne do
    @moduledoc """
    First puzzle
    """

    @spec run([String.t()]) :: integer()
    def run(input) do
      input
      |> TrenchMap.parse()
      |> TrenchMap.step(2)
      |> TrenchMap.number_of_light()
    end
  end

  defmodule PartTwo do
    @moduledoc """
    Second puzzle
    """

    @spec run([String.t()]) :: integer()
    def run(input) do
      input
      |> TrenchMap.parse()
      |> TrenchMap.step(50)
      |> TrenchMap.number_of_light()
    end
  end

  @spec parse([String.t()]) :: {algorithm(), trench_map(), pixel()}
  def parse([algorithm, "" | image]) do
    {parse_algorithm(algorithm), parse_image(image), :dark}
  end

  defp parse_algorithm(input) do
    0..(String.length(input) - 1)
    |> Stream.map(&{&1, pixel(String.at(input, &1))})
    |> Map.new()
  end

  defp parse_image(input) do
    height = length(input)
    width = String.length(List.first(input))

    0..(height - 1)
    |> Enum.reduce(%{}, fn y, acc ->
      line = Enum.at(input, y)

      0..(width - 1)
      |> Enum.reduce(acc, fn x, acc ->
        Map.put(acc, {y, x}, pixel(String.at(line, x)))
      end)
    end)
  end

  defp pixel("."), do: :dark
  defp pixel("#"), do: :light
  defp number(:dark), do: 0
  defp number(:light), do: 1
  defp symbol(:dark), do: "."
  defp symbol(:light), do: "#"

  @spec step({algorithm(), trench_map(), pixel()}, integer()) ::
          {algorithm(), trench_map(), pixel()}
  def step(data, 0), do: data

  def step({algorithm, trench_map, uncharted_pixel}, steps) do
    {min_x, max_x} = get_minmax_position(trench_map, 1)
    {min_y, max_y} = get_minmax_position(trench_map, 0)

    trench_map =
      (min_y - 1)..(max_y + 1)
      |> Enum.reduce(%{}, fn y, acc ->
        (min_x - 1)..(max_x + 1)
        |> Enum.reduce(acc, fn x, acc ->
          Map.put(acc, {y, x}, calculate(trench_map, y, x, algorithm, uncharted_pixel))
        end)
      end)

    # |> print()

    uncharted_pixel = calculate(trench_map, min_x - 3, min_y - 3, algorithm, uncharted_pixel)

    step({algorithm, trench_map, uncharted_pixel}, steps - 1)
  end

  defp get_minmax_position(trench_map, axis) do
    trench_map
    |> Stream.map(&elem(&1, 0))
    |> Stream.map(&elem(&1, axis))
    |> Enum.min_max()
  end

  defp calculate(trench_map, y, x, algorithm, uncharted_pixel) do
    i =
      [
        Map.get(trench_map, {y - 1, x - 1}, uncharted_pixel),
        Map.get(trench_map, {y - 1, x}, uncharted_pixel),
        Map.get(trench_map, {y - 1, x + 1}, uncharted_pixel),
        Map.get(trench_map, {y, x - 1}, uncharted_pixel),
        Map.get(trench_map, {y, x}, uncharted_pixel),
        Map.get(trench_map, {y, x + 1}, uncharted_pixel),
        Map.get(trench_map, {y + 1, x - 1}, uncharted_pixel),
        Map.get(trench_map, {y + 1, x}, uncharted_pixel),
        Map.get(trench_map, {y + 1, x + 1}, uncharted_pixel)
      ]
      |> Stream.map(&number/1)
      |> Enum.join()
      |> String.to_integer(2)

    Map.fetch!(algorithm, i)
  end

  @spec print(trench_map()) :: trench_map()
  def print(map) do
    {min_x, max_x} = get_minmax_position(map, 1)
    {min_y, max_y} = get_minmax_position(map, 0)

    for y <- min_y..max_y do
      for x <- min_x..max_x do
        "" <> symbol(Map.get(map, {y, x}))
      end
      |> Enum.join("")
    end
    |> Enum.join("\n")
    |> IO.puts()

    IO.puts("\n")

    map
  end

  @spec number_of_light({algorithm(), trench_map(), pixel()}) :: integer()
  def number_of_light({_, trench_map, _}) do
    trench_map
    |> Stream.filter(fn {_l, v} -> v == :light end)
    |> Enum.to_list()
    |> length()
  end
end
