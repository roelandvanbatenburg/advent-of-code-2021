defmodule TrickShot do
  @moduledoc """
  Read the packet
  """

  @type range :: {Range.t(), Range.t()}
  @type velocity :: {integer(), integer()}
  @type position :: {integer(), integer()}

  defmodule PartOne do
    @moduledoc """
    First puzzle
    """

    @spec run(String.t()) :: integer()
    def run(input) do
      range = TrickShot.parse(input)

      0..100
      |> Enum.map(&find_highest(range, &1))
      |> Enum.max()
    end

    defp find_highest(range, y) do
      0..100
      |> Enum.map(&find_highest_for_y(range, y, &1))
      |> Enum.max()
    end

    defp find_highest_for_y(range, y, x) do
      case TrickShot.comes_within_range?({x, y}, range) do
        nil -> -10_000_000
        h -> h
      end
    end
  end

  defmodule PartTwo do
    @moduledoc """
    Second puzzle
    """

    @spec run(String.t()) :: integer()
    def run(input) do
      range = TrickShot.parse(input)

      -100..100
      |> Enum.flat_map(&calculate(range, &1))
      |> Enum.reject(&is_nil/1)
      |> length()
    end

    defp calculate(range, y) do
      0..300
      |> Enum.map(&TrickShot.comes_within_range?({&1, y}, range))
    end
  end

  @spec parse(String.t()) :: {Range.t(), Range.t()}
  def parse("target area: x=" <> rest) do
    [x, y] = String.split(rest, ", y=")
    {range(x), range(y)}
  end

  defp range(input) do
    [first, last] = String.split(input, "..")
    Range.new(String.to_integer(first), String.to_integer(last))
  end

  @spec comes_within_range?(velocity(), range(), position(), integer()) :: nil | integer()
  def comes_within_range?(velocity, range, position \\ {0, 0}, highest \\ 0)

  def comes_within_range?({x_diff, y_diff} = velocity, range, {x, y} = position, highest) do
    if beyond?(position, range) do
      nil
    else
      if in?(position, range) do
        highest
      else
        comes_within_range?(new(velocity), range, {x + x_diff, y + y_diff}, max(highest, y))
      end
    end
  end

  def beyond?({x, y}, {_min_x..max_x, min_y.._max_y}), do: x > max_x or y < min_y

  def in?({x, y}, {min_x..max_x, min_y..max_y}),
    do: x >= min_x and x <= max_x and y >= min_y and y <= max_y

  defp new({0, y}), do: {0, y - 1}
  defp new({x, y}), do: {x - 1, y - 1}
end
