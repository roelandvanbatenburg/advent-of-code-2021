defmodule Whales do
  @moduledoc """
  Escape them
  """

  defmodule PartOne do
    @moduledoc """
    First puzzle
    """

    @spec run([integer()]) :: integer()
    def run(input) do
      input
      |> Whales.position_map()
      |> fuel_costs_per_position()
      |> Enum.min()
    end

    defp fuel_costs_per_position(position_map) do
      position_map
      |> Enum.map(&fuel_costs(position_map, elem(&1, 0)))
    end

    defp fuel_costs(position_map, position) do
      Enum.reduce(position_map, 0, &(&2 + elem(&1, 1) * abs(elem(&1, 0) - position)))
    end
  end

  defmodule PartTwo do
    @moduledoc """
    Second puzzle
    """

    @spec run([integer()]) :: integer()
    def run(input) do
      input
      |> Whales.position_map()
      |> fuel_costs_per_position()
      |> Enum.min()
    end

    defp fuel_costs_per_position(position_map) do
      {max_position, _crabs} = Enum.max_by(position_map, &elem(&1, 0))

      0..max_position
      |> Enum.map(&fuel_costs(position_map, &1))
    end

    defp fuel_costs(position_map, position) do
      Enum.reduce(position_map, 0, fn {from_position, crabs}, acc ->
        steps = abs(from_position - position)
        cost_per_crab = Enum.sum(0..steps)
        acc + crabs * cost_per_crab
      end)
    end
  end

  @spec position_map([integer()]) :: %{integer() => integer()}
  def position_map(crabs) do
    Enum.reduce(crabs, %{}, &Map.update(&2, &1, 1, fn crab -> crab + 1 end))
  end
end
