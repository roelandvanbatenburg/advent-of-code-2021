defmodule BinaryDiagnosticsTest do
  use ExUnit.Case

  alias BinaryDiagnostics.{PartOne, PartTwo}

  setup _context do
    %{
      input: """
        00100
        11110
        10110
        10111
        10101
        01111
        00111
        11100
        10000
        11001
        00010
        01010
      """
    }
  end

  test "part one", %{input: input} do
    assert 198 ==
             input
             |> String.trim()
             |> String.split("\n")
             |> Enum.map(&String.trim/1)
             |> PartOne.run()
  end

  test "part two", %{input: input} do
    assert 230 ==
             input
             |> String.trim()
             |> String.split("\n")
             |> Enum.map(&String.trim/1)
             |> PartTwo.run()
  end
end
