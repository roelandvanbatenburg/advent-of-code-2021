defmodule ChitonTest do
  use ExUnit.Case

  alias Chiton.{PartOne, PartTwo}

  setup _context do
    %{
      input: """
      1163751742
      1381373672
      2136511328
      3694931569
      7463417111
      1319128137
      1359912421
      3125421639
      1293138521
      2311944581
      """
    }
  end

  test "part one", %{input: input} do
    assert 40 ==
             input
             |> String.trim()
             |> String.split("\n")
             |> Enum.map(&String.trim/1)
             |> PartOne.run()
  end

  test "part two", %{input: input} do
    assert 315 ==
             input
             |> String.trim()
             |> String.split("\n")
             |> Enum.map(&String.trim/1)
             |> PartTwo.run()
  end
end
