defmodule DiracDiceTest do
  use ExUnit.Case

  alias DiracDice.{PartOne, PartTwo}

  setup _context do
    %{input: {4, 8}}
  end

  test "part one", %{input: input} do
    assert 739_785 == PartOne.run(input)
  end

  @tag :skip
  test "part two", %{input: input} do
    assert 444_356_092_776_315 == PartTwo.run(input)
  end
end
