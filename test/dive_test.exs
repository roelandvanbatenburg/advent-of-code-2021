defmodule DiveTest do
  use ExUnit.Case

  alias Dive.{PartOne, PartTwo}

  test "where are we?" do
    input = """
        forward 5
        down 5
        forward 8
        up 3
        down 8
        forward 2
    """

    assert 150 ==
             input
             |> String.trim()
             |> String.split("\n")
             |> Enum.map(&String.trim/1)
             |> PartOne.run()
  end

  test "here we are" do
    input = """
        forward 5
        down 5
        forward 8
        up 3
        down 8
        forward 2
    """

    assert 900 ==
             input
             |> String.trim()
             |> String.split("\n")
             |> Enum.map(&String.trim/1)
             |> PartTwo.run()
  end
end
