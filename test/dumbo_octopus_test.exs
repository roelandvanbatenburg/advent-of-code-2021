defmodule DumboOctopusTest do
  use ExUnit.Case

  alias DumboOctopus.{PartOne, PartTwo}

  setup _context do
    %{
      input: """
      5483143223
      2745854711
      5264556173
      6141336146
      6357385478
      4167524645
      2176841721
      6882881134
      4846848554
      5283751526
      """
    }
  end

  test "part one", %{input: input} do
    assert 1656 ==
             input
             |> String.trim()
             |> String.split("\n")
             |> Stream.map(&String.graphemes/1)
             |> Stream.map(fn line -> Enum.map(line, &String.to_integer/1) end)
             |> Enum.to_list()
             |> PartOne.run()
  end

  test "part two", %{input: input} do
    assert 195 ==
             input
             |> String.trim()
             |> String.split("\n")
             |> Stream.map(&String.graphemes/1)
             |> Stream.map(fn line -> Enum.map(line, &String.to_integer/1) end)
             |> Enum.to_list()
             |> PartTwo.run()
  end
end
