defmodule ExtendedPolymerizationTest do
  use ExUnit.Case

  alias ExtendedPolymerization.{PartOne, PartTwo}

  setup _context do
    %{
      input: """
      NNCB

      CH -> B
      HH -> N
      CB -> H
      NH -> C
      HB -> C
      HC -> B
      HN -> C
      NN -> C
      BH -> H
      NC -> B
      NB -> B
      BN -> B
      BB -> N
      BC -> B
      CC -> N
      CN -> C
      """
    }
  end

  test "part one", %{input: input} do
    assert 1588 ==
             input
             |> String.split("\n")
             |> Stream.map(&String.trim/1)
             |> Enum.join("\n")
             |> PartOne.run()
  end

  test "part two", %{input: input} do
    assert 2_188_189_693_529 ==
             input
             |> String.split("\n")
             |> Stream.map(&String.trim/1)
             |> Enum.join("\n")
             |> PartTwo.run()
  end
end
