defmodule HydrothermalVentureTest do
  use ExUnit.Case

  alias HydrothermalVenture.{PartOne, PartTwo}

  setup _context do
    %{
      input: """
      0,9 -> 5,9
      8,0 -> 0,8
      9,4 -> 3,4
      2,2 -> 2,1
      7,0 -> 7,4
      6,4 -> 2,0
      0,9 -> 2,9
      3,4 -> 1,4
      0,0 -> 8,8
      5,5 -> 8,2
      """
    }
  end

  test "part one", %{input: input} do
    assert 5 ==
             input
             |> String.trim()
             |> String.split("\n")
             |> Enum.map(&String.trim/1)
             |> PartOne.run()
  end

  test "part two", %{input: input} do
    assert 12 ==
             input
             |> String.trim()
             |> String.split("\n")
             |> Enum.map(&String.trim/1)
             |> PartTwo.run()
  end
end
