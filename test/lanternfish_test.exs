defmodule LanternfishTest do
  use ExUnit.Case

  alias Lanternfish.{PartOne, PartTwo}

  setup _context do
    %{input: [3, 4, 3, 1, 2]}
  end

  test "part one", %{input: input} do
    assert 5934 == PartOne.run(input)
  end

  test "part two", %{input: input} do
    assert 26_984_457_539 == PartTwo.run(input)
  end
end
