defmodule PacketDecoderTest do
  use ExUnit.Case

  alias PacketDecoder.{PartOne, PartTwo}

  test "part one" do
    assert 6 == PartOne.run("D2FE28")
    assert 9 == PartOne.run("38006F45291200")
    assert 14 == PartOne.run("EE00D40C823060")
    assert 16 == PartOne.run("8A004A801A8002F478")
    assert 12 == PartOne.run("620080001611562C8802118E34")
    assert 23 == PartOne.run("C0015000016115A2E0802F182340")
    assert 31 == PartOne.run("A0016C880162017C3686B18A3D4780")
  end

  test "part two" do
    assert 3 == PartTwo.run("C200B40A82")
    assert 54 == PartTwo.run("04005AC33890")
    assert 7 == PartTwo.run("880086C3E88112")
    assert 9 == PartTwo.run("CE00C43D881120")
    assert 1 == PartTwo.run("D8005AC2A8F0")
    assert 0 == PartTwo.run("F600BC2D8F")
    assert 0 == PartTwo.run("9C005AC2F8F0")
    assert 1 == PartTwo.run("9C0141080250320F1802104A08")
  end
end
