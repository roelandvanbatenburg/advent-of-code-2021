defmodule PassagePathingTest do
  use ExUnit.Case

  alias PassagePathing.{PartOne, PartTwo}

  setup _context do
    %{
      input: """
      start-A
      start-b
      A-c
      A-b
      b-d
      A-end
      b-end
      """
    }
  end

  test "part one", %{input: input} do
    assert 10 ==
             input
             |> String.trim()
             |> String.split("\n")
             |> Enum.map(&String.trim/1)
             |> PartOne.run()
  end

  test "part two", %{input: input} do
    assert 36 ==
             input
             |> String.trim()
             |> String.split("\n")
             |> Enum.map(&String.trim/1)
             |> PartTwo.run()

    input = """
    dc-end
    HN-start
    start-kj
    dc-start
    dc-HN
    LN-dc
    HN-end
    kj-sa
    kj-HN
    kj-dc
    """

    assert 103 ==
             input
             |> String.trim()
             |> String.split("\n")
             |> Enum.map(&String.trim/1)
             |> PartTwo.run()
  end
end
