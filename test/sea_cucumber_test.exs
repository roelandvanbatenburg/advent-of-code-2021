defmodule SeaCucumberTest do
  use ExUnit.Case

  alias SeaCucumber.PartOne

  setup _context do
    %{
      input: [
        "v...>>.vv>\n",
        ".vv>>.vv..\n",
        ">>.>v>...v\n",
        ">>v>>.>.v.\n",
        "v>v.vv.v..\n",
        ">.>>..v...\n",
        ".vv..>.>v.\n",
        "v.v..>>v.v\n",
        "....v..v.>\n"
      ]
    }
  end

  test "part one", %{input: input} do
    assert 58 == PartOne.run(input)
  end
end
