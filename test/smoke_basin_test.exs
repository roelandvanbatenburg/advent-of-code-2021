defmodule SmokeBasinTest do
  use ExUnit.Case

  alias SmokeBasin.{PartOne, PartTwo}

  setup _context do
    %{
      input: """
      2199943210
      3987894921
      9856789892
      8767896789
      9899965678
      """
    }
  end

  test "part one", %{input: input} do
    assert 15 ==
             input
             |> String.trim()
             |> String.split("\n")
             |> Enum.map(&String.trim/1)
             |> PartOne.run()
  end

  test "part two", %{input: input} do
    assert 1134 ==
             input
             |> String.trim()
             |> String.split("\n")
             |> Enum.map(&String.trim/1)
             |> PartTwo.run()
  end
end
