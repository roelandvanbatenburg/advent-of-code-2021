defmodule SonarSweepTest do
  use ExUnit.Case

  alias SonarSweep

  test "find the depth" do
    input = [199, 200, 208, 210, 200, 207, 240, 269, 260, 263]

    assert 7 == SonarSweep.run_1(input)
  end

  test "find the depth with a sliding window" do
    input = [199, 200, 208, 210, 200, 207, 240, 269, 260, 263]

    assert 5 == SonarSweep.run_2(input)
  end
end
