defmodule SyntaxScoringTest do
  use ExUnit.Case

  alias SyntaxScoring.{PartOne, PartTwo}

  setup _context do
    %{
      input: [
        "[({(<(())[]>[[{[]{<()<>>\n",
        "[(()[<>])]({[<{<<[]>>(\n",
        "{([(<{}[<>[]}>{[]{[(<()>\n",
        "(((({<>}<{<{<>}{[]{[]{}\n",
        "[[<[([]))<([[{}[[()]]]\n",
        "[{[{({}]{}}([{[{{{}}([]\n",
        "{<[[]]>}<{[{[{[]{()[[[]\n",
        "[<(<(<(<{}))><([]([]()\n",
        "<{([([[(<>()){}]>(<<{{\n",
        "<{([{{}}[<[[[<>{}]]]>[]]\n"
      ]
    }
  end

  test "part one", %{input: input} do
    assert 26_397 == PartOne.run(input)
  end

  test "part two", %{input: input} do
    assert 288_957 == PartTwo.run(input)
  end
end
