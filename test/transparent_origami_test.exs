defmodule TransparentOrigamiTest do
  use ExUnit.Case

  alias TransparentOrigami.{PartOne, PartTwo}

  setup _context do
    %{
      input: """
      6,10
      0,14
      9,10
      0,3
      10,4
      4,11
      6,0
      6,12
      4,1
      0,13
      10,12
      3,4
      3,0
      8,4
      1,10
      2,14
      8,10
      9,0

      fold along y=7
      fold along x=5
      """
    }
  end

  test "part one", %{input: input} do
    assert 17 ==
             input
             |> String.split("\n")
             |> Stream.map(&String.trim/1)
             |> Enum.join("\n")
             |> PartOne.run()
  end

  test "part two", %{input: input} do
    assert 16 ==
             input
             |> String.split("\n")
             |> Stream.map(&String.trim/1)
             |> Enum.join("\n")
             |> PartTwo.run()
  end
end
