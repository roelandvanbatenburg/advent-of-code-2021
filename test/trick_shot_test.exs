defmodule TrickShotTest do
  use ExUnit.Case

  alias TrickShot.{PartOne, PartTwo}

  test "part one" do
    range = TrickShot.parse("target area: x=20..30, y=-10..-5")
    assert 3 == TrickShot.comes_within_range?({7, 2}, range)
    assert 6 == TrickShot.comes_within_range?({6, 3}, range)
    assert 0 == TrickShot.comes_within_range?({9, 0}, range)
    refute TrickShot.comes_within_range?({17, -4}, range)
    assert 45 == PartOne.run("target area: x=20..30, y=-10..-5")
  end

  test "part two" do
    assert 112 == PartTwo.run("target area: x=20..30, y=-10..-5")
  end
end
