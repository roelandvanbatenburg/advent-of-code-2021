defmodule WhalesTest do
  use ExUnit.Case

  alias Whales.{PartOne, PartTwo}

  setup _context do
    %{input: [16, 1, 2, 0, 4, 2, 7, 1, 2, 14]}
  end

  test "part one", %{input: input} do
    assert 37 == PartOne.run(input)
  end

  test "part two", %{input: input} do
    assert 168 == PartTwo.run(input)
  end
end
